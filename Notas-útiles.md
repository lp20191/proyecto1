* Pueden haber 0 o más partes *elif*, y la parte *else* es opcional.

## Gramática de las declaraciones if
if_stmt ::=  "if" expression ":" suite
             ("elif" expression ":" suite)*
             ["else" ":" suite]
