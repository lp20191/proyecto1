# proyecto1 - Lenguajes de Programación 2019-1

## Integrantes

* Angélica Tejeda
* Adrián Tomalá
* Juan Diego Vallejo

## Primer Avance (5ta semana)

1. Definir los posibles ejemplos de tareas que se podría reconocer en su analizador. Proponer al menos 3 ejercicios: desde una tarea sencilla, una de nivel intermedio, y una compleja. [x]
2. Usando el módulo lex, defina todos los tokens a reconocer en su analizador léxico. No olvidar ningún operador permitido en Python. [x]

## Segundo Avance (6ta semana)
1. Usando el módulo jacc, defina las gramáticas con respecto a los ejercicios que su analizador es capaz de reconocer y según su tema asignado. [x]

## Tercer Avance (7ma semana)
1. Diseñe una interfaz gráfica para su analizador de código, donde pueda escribir el código base, el código plagiado, los botones de análisis y área de resultados.

2. Realice el análisis de similitud utilizando un tipo de estructura de datos, que le permita evaluar los árboles sintácticos de cada fragmento de código y determine el posible plagio. Para mejor detalle, imprima un árbol sintáctico por cada fragmento de código y su porcentaje de similitud.
