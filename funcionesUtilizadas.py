def cambio_tupla_lista(tupla, lista):
    if tupla is None:
        print('yes')
    a=list(tupla)
    if type(a) is list:
        for i in a:
            if type(i)==type(tupla):
                cambio_tupla_lista(i, lista)
            else:
                lista.append(i)

def porcentajePlagio(tupla1, tupla2):
        lista1=[]
        lista2=[]
        coincidencias=0
        cambio_tupla_lista(tupla1, lista1)
        cambio_tupla_lista(tupla2, lista2)
        if len(lista1)<len(lista2):
            max=len(lista1)
        elif len(lista1)>len(lista2):
            max=len(lista2)
        else:
            max=len(lista1)
        for i in range(max):
            if lista1[i]==lista2[i]:
                coincidencias+=1
            else:
                if(type(lista1[i] is str ) and lista1[i-2] == "SENTENCIA"):
                    coincidencias+=1
                if(type(lista1[i] is int ) and lista1[i-3] == "SENTENCIA"):
                    coincidencias+=1
        plagio = round((coincidencias/len(lista1))*100,2)
        return "Porcentaje de plagio encontrado:"+str(plagio)+"%"
