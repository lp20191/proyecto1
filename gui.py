from tkinter import *
from tkinter.ttk import Frame, Label, Entry
from lexer import *
from funcionesUtilizadas import *
from parser import *
from parser import lista_errores as l_err
import time


root = Tk()
root.geometry("960x960")
root.title("Analizador de Plagio")
root.configure(background="lightBlue")

var = StringVar()
label1 = Label(root, textvariable=var, relief=RAISED)
var.set("Codigo 1")
label1.place(x=48, y=48, height=48, width=408)
text1 = Text(root)
text1.pack(padx=0, side=LEFT)
text1.place(x=48, y=96, height=200, width=408)

var2 = StringVar()
label2 = Label(root, textvariable=var2, relief=RAISED)
var2.set("Codigo2")
label2.place(x=504, y=48, height=48, width=408)
text2 = Text(root)
text2.pack(padx=0, side=RIGHT)
text2.place(x=504, y=96, height=200, width=408)

var3 = StringVar()
label3 = Label(root, textvariable=var3, relief=RAISED)
var3.set("Analisis de códigos")
label3.place(x=48, y=320, height=48, width=864)
resul = Text(root, state='disabled')
resul.pack(padx=0, side=LEFT)
resul.place(x=48, y=368, height=200, width=864)

sintactico_btn = Button(root, text="Análisis \n sintáctico", bg="gray", command=lambda: sintacticoInput())
sintactico_btn.pack(side=TOP, pady=100)
sintactico_btn.place(x=48, y=600, height=50, width=250)

lexico_btn = Button(root, text="Análisis \n léxico", bg="gray", command=lambda: lexicoInput())
lexico_btn.pack(side=TOP, pady=50)
lexico_btn.place(x=355, y=600, height=50, width=250)

plagio_btn = Button(root, text="Analisis \n Plagio", bg="gray", command=lambda: plagioInput())
plagio_btn.pack(side=TOP, pady=100)
plagio_btn.place(x=662, y=600, height=50, width=250)


def lexicoInput():
    resul.delete(1.0, END)
    inputValue1 = text1.get("1.0", "end-1c")
    inputValue2 = text2.get("1.0", "end-1c")
    cadena1 = inputValue1.strip()
    cadena2 = inputValue2.strip()
    lista1 = show_tokens(cadena1)
    lista2 = show_tokens(cadena2)
    resul.config(state="normal")
    inicio ='\n\n' + "ANALISIS LÉXICO".center(40, " ") + '\n\n'
    archivo = open("reportes/AnalisisLexico_" + time.strftime("%d_%m_%y") + "-" + time.strftime("%H_%M_%S") + ".txt",
                   "w")
    resul.insert('end', inicio)
    archivo.write(inicio)
    resul.insert('end', "Código 1:\n")
    archivo.write("Código 1:\n")
    for i in lista1:
        resul.insert('end', i + "\n")
        archivo.write(i + "\n")
    resul.insert('end', "\nCódigo 2:\n")
    archivo.write("Código 2:\n")
    for i in lista2:
        resul.insert('end', i + "\n")
        archivo.write(i + "\n")
    archivo.close()
    resul.configure(state='disabled')

def sintacticoInput():
    inputValue1 = text1.get("1.0", "end-1c")
    inputValue2 = text2.get("1.0", "end-1c")
    cadena1 = inputValue1.strip()
    cadena2 = inputValue2.strip()
    tupla1 = ejecutar_yacc(cadena1)
    len1 = len(l_err)
    tupla2 = ejecutar_yacc(cadena2)
    len2 = len(l_err)
    resul.config(state="normal")
    inicio = '\n\n' + "ANALISIS SINTÁCTICO".center(40, " ") + '\n\n'
    file = open("reportes/AnalisisSintactico_" + time.strftime("%d_%m_%y") + "-" + time.strftime("%H_%M_%S")
                   + ".txt", "w")
    resul.insert('end', inicio)
    file.write(inicio)
    resul.insert('end', "Codigo 1:\n")
    file.write("Codigo 1:\n")
    resul.insert('end', str(tupla1) + "\n")
    file.write(str(tupla1) + "\n")

    if len1 > 0:
        resul.insert('end', "Errores en programa 1:\n")
        file.write("Errores en programa 1:\n")
        for i in range(len1):
            resul.insert('end', l_err[i] + "\n")
            file.write(l_err[i] + "\n")
    resul.insert('end', "Codigo 2:\n")
    file.write("Codigo 2:\n")
    resul.insert('end', str(tupla2) + "\n")
    file.write(str(tupla2) + "\n")

    if (len2 - len1) > 0:
        resul.insert('end', "Errores en programa 2:\n")
        file.write("Errores en programa 2:\n")
        for i in range(len2 - len1):
            resul.insert('end', l_err[i + len1] + "\n")
            file.write(l_err[i + len1] + "\n")
    resul.configure(state='disabled')
    file.close()



def plagioInput():
    inputValue1 = text1.get("1.0", "end-1c")
    inputValue2 = text2.get("1.0", "end-1c")
    str1 = inputValue1.strip()
    str2 = inputValue2.strip()
    tupla1 = ejecutar_yacc(str1)
    tupla2 = ejecutar_yacc(str2)
    resultado = porcentajePlagio(tupla1, tupla2)
    resul.config(state="normal")
    contenido = '\n' + '~' * 40 + '\n' + resultado.center(40, " ") + '\n' + '~' * 40 + '\n'
    archivo = open("reportes/AnalisisDePlagio_" + time.strftime("%d_%m_%y") + "-" + time.strftime("%H_%M_%S") + ".txt",
                   "w")
    archivo.write(contenido)
    archivo.close()
    resul.insert('end', contenido)
    resul.configure(state='disabled')

mainloop()
