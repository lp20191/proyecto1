import ply.lex as lex

palabras_reservadas = {
  'if' : 'IF',  'then' : 'THEN',  'else' : 'ELSE',  'elif' : 'ELIF',
  'while' : 'WHILE',  'for' : 'FOR',  'and' : 'AND',  'or' : 'OR',
  'not' : 'NOT',  'True': 'TRUE',  'False': 'FALSE',  'in': 'IN',
  'is': 'IS',  'return':'RETURN',  'break':'BREAK',  'as':'AS',
  'import':'IMPORT',  'def':'DEF',  'print':'PRINT',  'none':'NONE',
  'list':'LIST',  'tuple':'TUPLE',  'dic':'DIC',  'from':'FROM',
  'set':'SET',  'range':'RANGE',  'len':'LEN', 'input':'INPUT',

}

tokens = ['NAME', 'NUMBER', 'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'EQUALS',
    'LPAREN','RPAREN', 'LESS','GREATER', 'DISTINCT',
'SEMI','COMMA','LBRACKET','RBRACKET','LBLOCK','RBLOCK','COLON','AMPERSAND','HASHTAG','DOT','QUOTES',
    'APOSTROPHE', 'LESSEQUAL', 'GREATEREQUAL', 'DEQUAL','ISEQUAL',
'MINUSMINUS', 'PLUSPLUS', 'TIMESTIMES', 'DOT_DOT', 'INT', 'COMMENT', 'MOD_PER', 'TEXT',]  + list(palabras_reservadas.values())

t_ignore = ' \t'
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_EQUALS = r'='
t_MOD_PER = r'%'
t_DISTINCT   = r'!'
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
# t_STRING = r'(("[^"]*")|(\'[^\']*\'))'
t_LESS      = r'<'
t_GREATER   = r'>'
t_SEMI      = r';'
t_COMMA     = r','
t_LBRACKET  = r'\['
t_RBRACKET  = r'\]'
t_LBLOCK    = r'{'
t_RBLOCK    = r'}'
t_COLON     = r':'
t_AMPERSAND = r'\&'
t_HASHTAG   = r'\#'
t_DOT       = r'\.'
t_QUOTES    = r'\"'
t_APOSTROPHE = r'\''

def t_NAME(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = palabras_reservadas.get(t.value, 'NAME')
    return t

def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t


def t_TRUE(t):
    r'true|True'
    return t

def t_FALSE(t):
    r'false|False'
    return t

def t_PRINT(t):
    r'print'
    return t

def t_LESSEQUAL(t):
    r'<='
    return t

def t_GREATEREQUAL(t):
    r'>='
    return t

def t_DEQUAL(t):
    r'!='
    return t

def t_ISEQUAL(t):
    r'=='
    return t

def t_MINUSMINUS(t):
    r'--'
    return t

def t_PLUSPLUS(t):
    r'\+\+'
    return t

def t_TIMESTIMES(t):
    r'\*\*'
    return t

def t_DOT_DOT(t):
    r'::'
    return t

def t_COMMENT(t):
    r'\/\*([^*]|\*[^\/])*(\*)+\/'
    t.lexer.lineno += t.value.count('\n')

def t_LIST(t):
    r'list'
    return t

def t_RETURN(t):
    r'return'
    return t

def t_INPUT(t):
    r'input'
    return t

def t_INT(t):
    r'int'
    return t


# Regla para rastrear números de línea
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# Regla para manejar errores
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

lexer = lex.lex() # Construye el lexer

def show_tokens(cadena):
    l=[]
    lexer.input(cadena)
    while True:
        tok = lexer.token()
        if not tok: break
        l.append(tok.type)
    return l
    #Formato de la salida: LexRoken(type, value, lineno, lexpos)

'''
print("-----EJEMPLO 1-----\n")

print("Input prueba--->\n\nn1 = int(input(\"Ingrese un numero entero\"))\nif n1 % 2 == 0:\n\tprint(\"El numero es par\")\nelse: print(\"El numero es impar\")")
lex.input("n1 = int(input(\"Ingrese un numero entero\"))\nif n1 % 2 == 0:\n\tprint(\"El numero es par\")\nelse: print(\"El numero es impar\")")
print("\n")
while True:
    tok = lex.token()
    if not tok: break
    print(str(tok.type) + " " + str(tok.value))

print("\n\n\n-----EJEMPLO 2-----\n")

print("Input prueba --->\n\ntotal = 100\ncountry = \"AU\"\nif country == \"US\":\n\tif total <= 50:\n\tprint \"Shipping Cost is  $50\"\nelif total <= 100:\n\tprint \"Shipping Cost is $25\"\nelif total <= 150:\n\tprint \"Shipping Costs $5\"\nelse:\n\tprint \"FREE\"\nif country == \"AU\":\n\tif total <= 50:\n\tprint \"Shipping Cost is  $100\"\nelse:\n\tprint \"FREE\"")
lex.input("total = 100\ncountry = \"AU\"\nif country == \"US\":\n\tif total <= 50:\n\tprint \"Shipping Cost is  $50\"\nelif total <= 100:\n\tprint \"Shipping Cost is $25\"\nelif total <= 150:\n\tprint \"Shipping Costs $5\"\nelse:\n\tprint \"FREE\"\nif country == \"AU\":\n\tif total <= 50:\n\tprint \"Shipping Cost is  $100\"\nelse:\n\tprint \"FREE\"")
print("\n")
while True:
    tok = lex.token()
    if not tok: break
    print(str(tok.type) + " " + str(tok.value))

print("\n\n\n-----EJEMPLO 3-----\n")

print("Input prueba--->\n\nyear = int(input(\"Ingrese un año: \"))\n\nif (year % 4) == 0:\n\tif (year % 100) == 0:\n\t\tif (year % 400) == 0:\n\t\t\tprint(\"{0} es año bisiesto\".format(year))\n\t\telse:\n\t\t\tprint(\"{0} no es año bisiesto\".format(year))\n\telse:\n\t\tprint(\"{0} es año bisiesto\".format(year))\nelse:\n\tprint(\"{0} no es año bisiesto\".format(year))")
lex.input("year = int(input(\"Ingrese un año: \"))\n\nif (year % 4) == 0:\n\tif (year % 100) == 0:\n\t\tif (year % 400) == 0:\n\t\t\tprint(\"{0} es año bisiesto\".format(year))\n\t\telse:\n\t\t\tprint(\"{0} no es año bisiesto\".format(year))\n\telse:\n\t\tprint(\"{0} es año bisiesto\".format(year))\nelse:\n\tprint(\"{0} no es año bisiesto\".format(year))")
print("\n")
while True:
    tok = lex.token()
    if not tok: break
    print(str(tok.type) + " " + str(tok.value))

'''
