import ply.yacc as yacc
import lexer

tokens = lexer.tokens
lista_errores=[]

def p_sentencia(p):
	'''sentencia : assign
	| for
	| while
	| if
	| assign sentencia
				 | for sentencia
				 | while sentencia
				 | if sentencia'''
	if(len(p)==2):
		p[0] = ('SENTENCIA',p[1])
	if(len(p)==3):
		p[0] = ('SENTENCIA',p[1],p[2])

def p_assign(p):
	'''assign : NAME EQUALS expr
			  | NAME EQUALS list
			  | NAME EQUALS cad
			  | NAME EQUALS input'''
	p[0] = (p[2],p[1],p[3])

"""
def p_assign(p):
	'''assign : NAME EQUALS expr
			  | NAME EQUALS list
			  | NAME EQUALS cad
			  | for
			  | while
			  | if
			  | NAME EQUALS expr assign
			  | NAME EQUALS list assign
			  | NAME EQUALS cad assign
			  | for assign
			  | while assign
			  | if assign'''

	if(len(p)==4):
		p[0] = (p[2],p[1],p[3])
	if(len(p)==2):
		p[0] = (p[1])
"""

def p_input(p):
	'''input : INPUT LPAREN cad RPAREN
	'''
	p[0] = ('INPUT')

def p_expr(p):
	'''expr : expr PLUS term
			| expr MINUS term
			| term'''
	if(len(p)==4):
		p[0] = (p[2], p[1],p[3])
	if(len(p)==2):
		p[0] = (p[1])

def p_term(p):
	'''term : term TIMES factor
			| term DIVIDE factor
			| term TIMESTIMES factor
			| term MOD_PER factor
			| factor'''
	if(len(p)==4):
		p[0] = (p[2],p[1],p[3])
	if(len(p)==2):
		p[0] = (p[1])



def p_list(p):
	'''list : LBRACKET RBRACKET
			| LBRACKET element RBRACKET
			| LBRACKET element elements RBRACKET'''
	"""
	if(len(p)==3):
		p[0] = ("")
	if(len(p)==4):
		p[0] = (p[2])
	if(len(p)==5):
		p[0] = (p[2],p[3])
	"""

	p[0] = ('LISTA')

def p_element(p):
	'''element : factor
			   | NAME
			   | list'''
	p[0] = (p[1])

def p_elements(p):
	'''elements : COMMA element
				| COMMA list
				| COMMA element elements'''
	if(len(p)==3):
		p[0] = (p[2])
	if(len(p)==4):
		p[0] = (p[2],p[3])

def p_factor(p):
	'''factor : NUMBER
			  | MINUS NUMBER
			  | NAME'''
	if(len(p)==3):
		p[0] = (p[1],p[2])
	if(len(p)==2):
		p[0] = (p[1])

def p_cad(p):
	'''cad : APOSTROPHE str APOSTROPHE
		   | QUOTES str QUOTES
		   | cad LBRACKET index RBRACKET
		     '''
	p[0] = ('CADENA')

def p_str(p):
	'''str : NAME
		   | NAME str'''

def p_index(p):
	'''index : factor
			 | factor COLON factor
			 | factor COLON factor COLON factor'''
	"""
	if(len(p)==2):
		p[0] = (p[1])
	if(len(p)==4):
		p[0] = (p[1],p[3])
	if(len(p)==6):
		p[0] = (p[1],p[3],p[5])
	"""

	p[0] = ('INDEX')

def p_while(p):
	'''while : WHILE LPAREN comparison RPAREN COLON sentencia
			 | WHILE comparison COLON sentencia
			 | WHILE LPAREN comparison comparisons RPAREN COLON sentencia
			 | WHILE comparison comparisons COLON sentencia'''
	if(len(p)==5):
		p[0] = (p[1],p[2],p[4])
	if(len(p)==6):
		p[0] = (p[1],p[2],p[3],p[5])
	if(len(p)==7):
		p[0] = (p[1],p[3],p[6])
	if(len(p)==8):
		p[0] = (p[1],p[3],p[4],p[7])

def p_print(p):
	'''print : PRINT LPAREN cad RPAREN
			| PRINT cad
			| PRINT LPAREN NUMBER RPAREN
			| PRINT NUMBER
			| PRINT LPAREN NAME RPAREN
			| PRINT NAME
	'''
	if (len (p)==5):
		p[0] = (p[1],p[3])
 	#if (len (p)==3 and p[2]=='cad'):
	#	p[0] = (p[1], p[2])
	if (len (p)==3):
		p[0] = (p[1])

def p_comparisons(p):
	'''comparisons : AND comparison
				   | AND comparison comparisons
				   | OR comparison
				   | OR comparison comparisons'''
	if(len(p)==3):
		p[0] = (p[1],p[2])
	if(len(p)==4):
		p[0] = (p[1],p[2],p[3])

def p_bool(p):
	'''bool : TRUE
			| FALSE'''
	p[0] = (p[1])


def p_comparison(p):
	'''comparison : comp ISEQUAL comp
				  | comp DEQUAL comp
				  | comp GREATER comp
				  | comp LESS comp
				  | comp GREATEREQUAL  comp
				  | comp LESSEQUAL comp
				  | bool'''
	if(len(p)==4):
		p[0] = (p[2],p[1],p[3])
	if(len(p)==2):
		p[0] = (p[1])

def p_comp(p):
	'''comp : expr
			| element
			| bool'''
	p[0] = (p[1])

def p_for(p):
	'''for : FOR NAME IN RANGE LPAREN r_value RPAREN COLON assign
		   | FOR NAME IN RANGE LPAREN r_values RPAREN COLON assign
		   | FOR NAME IN NAME COLON assign'''
	if(len(p)==9):
		p[0] = (p[2],p[3],p[4])
	if(len(p)==6):
		p[0] = (p[2],p[4])

def p_r_values(p):
	'''r_values : r_value COMMA r_value
				| r_value COMMA r_value COMMA r_value'''
	if(len(p)==4):
		p[0] = (p[1],p[2])
	if(len(p)==6):
		p[0] = (p[1],p[3],p[4])

def p_r_value(p):
	'''r_value : factor
			   | LEN LPAREN NAME RPAREN'''
	if(len(p)==2):
		p[0] = (p[1])
	if(len(p)==5):
		p[0] = (p[1],p[3])

def p_if(p):
	'''if : IF LPAREN comparison RPAREN COLON sentencia
		  | IF LPAREN comparison comparisons RPAREN COLON sentencia
		  | IF comparison COLON sentencia
		  | IF comparison comparisons COLON sentencia
		  | IF LPAREN comparison RPAREN COLON sentencia ELSE COLON sentencia
		  | IF LPAREN comparison comparisons RPAREN COLON sentencia ELSE COLON sentencia
		  | IF comparison COLON sentencia ELSE COLON sentencia
		  | IF comparison comparisons COLON sentencia ELSE COLON sentencia
		  | IF LPAREN comparison RPAREN COLON sentencia ELIF LPAREN comparison RPAREN COLON sentencia ELSE COLON sentencia
		  | IF LPAREN comparison comparisons RPAREN COLON sentencia ELIF LPAREN comparison comparisons RPAREN COLON sentencia ELSE COLON sentencia
		  | IF comparison COLON sentencia ELIF comparison COLON sentencia ELSE COLON sentencia
		  | IF comparison comparisons COLON sentencia ELIF comparison comparisons COLON sentencia ELSE COLON sentencia
		  | IF LPAREN comparison RPAREN COLON sentencia ELIF LPAREN comparison RPAREN COLON sentencia
		  | IF LPAREN comparison comparisons RPAREN COLON sentencia ELIF LPAREN comparison comparisons RPAREN COLON sentencia
		  | IF comparison COLON sentencia ELIF comparison COLON sentencia
		  | IF comparison comparisons COLON sentencia ELIF comparison comparisons COLON sentencia'''
	if(len(p)==5):
		p[0] = (p[1],p[2],p[4])
	if(len(p)==6):
		p[0] = (p[1],p[2],p[3],p[5])
	if(len(p)==7):
		p[0] = (p[1],p[3],p[6])
	if(len(p)==8):
		p[0] = (p[1],p[3],p[4],p[7])
	if(len(p)==8 and p[5]=="else"):
		p[0] = (p[1],p[2],p[4],p[5],p[7])
	if(len(p)==9):
		p[0] = (p[1],p[2],p[3],p[5],p[6],p[8])
	if (len(p) == 9 and p[5] == 'elif'):
		p[0] = (p[1], p[2], p[4], p[5], p[6], p[8])
	if(len(p)==10):
		p[0] = (p[1],p[3],p[6],p[7],p[9])
	if(len(p)==11):
		p[0] = (p[1],p[3],p[4],p[7],p[8],p[10])
	if(len(p)==11 and p[6]=='elif'):
		p[0] = (p[1],p[2],p[3],p[5],p[6],p[7],p[8],p[10])
	if(len(p)==12):
		p[0] = (p[1],p[2],p[4],p[5],p[6],p[8],p[9],p[11])
	if(len(p)==13):
		p[0] = (p[1],p[3],p[6],p[7],p[9],p[12])
	if(len(p)==14):
		p[0] = (p[1],p[2],p[3],p[5],p[6],p[7],p[8],p[10],p[11],p[13])
	if (len(p) == 15):
		p[0] = (p[1], p[3], p[4], p[7], p[8], p[10], p[11], p[14])
	if(len(p)==16):
		p[0] = (p[1],p[3],p[6],p[7],p[9],p[12],p[13],p[15])
	if(len(p)==18):
		p[0] = (p[1],p[3],p[4],p[7],p[8],p[10],p[11],p[14],p[15],p[17])



def p_error(p):
    if p is not None:
        lista_errores.append(p.type)
    else:
        print("Input vacío. Error.")

yacc.yacc()

def ejecutar_yacc(s):
	result = yacc.parse(s)
	return result
